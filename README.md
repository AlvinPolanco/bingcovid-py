# NovelCovid

A python Discord bot to report some findings of the COVID-19 virus, using the NovelCOVID-19 API.

## Pre-reqs

It is best to use virtual env. Once started use `python3 -m pip install -r requirements.txt`. 

## Use

1. Create a Discord bot through the [Discord Developer portal](https://discordapp.com/developers/applications/).
1. Copy unique Discord key to a file named `.env` with `export DISCORD_TOKEN=<key>` as it's contents. 
1. `source .env`
1. Start bot

 
 covidbot accepts 6 arguments:

`ncint` -Returns "Total Global" statistics

`ncint countryname` -Returns "Total Country" statistics

`ncint TopX` -Returns Top X countries based on total cases

`ncus` -Returns "Total United States" statistics

`ncus statename` -Returns "Total US State" statistics, using full statename or 2-letter state abreviation

`ncus statename countyname` -Returns "Total County" stats within given state, using 2-letter state abbreviation

`ncus TopX` -Returns Top X states based on total cases


## Changes

| Date           | Change                                                         |
|----------------|----------------------------------------------------------------|
| April 8, 2020  | Converted into Discord bot.                                    |
| April 8, 2020  | Added US counties within a state.                              |
| April 11, 2020 | Added numerical commas and iconography.                        |
| April 12, 2020 | Added country flags.                                           |
| April 14, 2020 | Added ability to find Top X in Countries or States.            |
| April 15, 2020 | Reformatted topx return and help menu to use formatted tables. |
| April 24, 2020 | Switched from Bing API to Novel COVID API.                     |
| April 24, 2020 | Changed main program filename, and lookup commands.            |
