from prettytable import PrettyTable
import locale
import requests
import discord
import json
import flag
import re
import os

locale.setlocale(locale.LC_ALL, "")
apiUrl = "https://corona.lmao.ninja/v2/"
TOKEN = os.getenv('DISCORD_TOKEN')

with open("states.json",'r') as statefile:
	statejson = json.load(statefile)

def novelOutput(name, region):
	outFormat = ""
	#separate all values and change any none/null values to 0
	outList = []
	outList.append(name)
	outList.append(region['cases'])
	outList.append(region['todayCases'])
	outList.append(region['deaths'])
	outList.append(region['todayDeaths'])
	try:
		outList.append(region['recovered'])
	except:
		outList.append(region['cases'] - region['active'])
	outList = [0 if x == None else x for x in outList]
	
	#return all values and add commas to number values
	outFormat = f"{outList[0]} Covid-19 Statistics\n"
	outFormat += "🤢 Total Cases: " + str(f"{outList[1]:n}")
	outFormat += " -- Today's Cases: " + str(f"{outList[2]:n}")
	outFormat += "  💀 Total Deaths: " + str(f"{outList[3]:n}")
	outFormat += " -- Today's Deaths: " + str(f"{outList[4]:n}")
	outFormat += "  😅 Total Recovered: " + str(f"{outList[5]:n}")
	return outFormat

def countyOutput(region):
	outFormat = ""
	#separate all values and change any none/null values to 0
	outList = []
	outList.append(f"{region['county']} County, {region['province']}")
	outList.append(region['stats']['confirmed'])
	outList.append(region['stats']['deaths'])
	outList.append(region['stats']['recovered'])
	outList = [0 if x == None else x for x in outList]
	
	#return all values and add commas to number values
	outFormat = f"{outList[0]} Covid-19 Statistics\n"
	outFormat += "🤢 Total Cases: " + str(f"{outList[1]:n}")
	outFormat += "  💀 Total Deaths: " + str(f"{outList[2]:n}")
	outFormat += "  😅 Total Recovered: " + str(f"{outList[3]:n}")
	return outFormat

def getTopX(region, topx):
	if topx == "":
		topx = "5"

	# table output formatting
	topTable = PrettyTable()
	topTable.field_names = [
		region,
		"Total Cases",
		"Today's Cases",
		"Total Deaths",
		"Today's Deaths"]
	topTable.align[region] = "l"
	topTable.align["Total Cases"] = "r"
	topTable.align["Today's Cases"] = "r"
	topTable.align["Total Deaths"] = "r"
	topTable.align["Today's Deaths"] = "r"

	url = f"{apiUrl}{region}?sort=cases"
	top = requests.get(url).json()

	if region == "States":
		sRegion = "state"
	elif region == "Countries":
		sRegion = "country"

	for i in range(int(topx)):
		topTable.add_row([
			top[i][sRegion],
			f"{top[i]['cases']:n}",
			f"{top[i]['todayCases']:n}",
			f"{top[i]['deaths']:n}",
			f"{top[i]['todayDeaths']:n}"
			])


	topXoutmsg = f'```\n{topTable.get_string(title=f"Covid-19 Top {topx} {region} by total cases")}```'
	return topXoutmsg

def usStateStats(state):
	stateOut = ""
	if len(state) == 2:
		try:
			state = statejson['states'][(state[:2])]
		except:
			return f"{state} is not a valid state appreviation.  Try again."

	url = apiUrl + "states/" + state
	try:
		i = requests.get(url).json()
		stateOut = novelOutput(i['state'],i)
	except:
		stateOut = state + " is not a valid US State name.  Try again."
	return stateOut

def usCountyStats(state, county):
	url = f"{apiUrl}jhucsse/counties/{county}"
	countyOut = ""
	getState = state.upper()
	try:
		getState = statejson['states'][(getState[:2])]
		i = requests.get(url).json()
		for c in i:
			if c['province'] == getState:
				countyOut = countyOutput(c)
	except:
		countyOut = f"{county} is not a valid county name.  Try again."
	return countyOut

def countryStats(country):
	countryOut = ""
	if country == "":
		url = apiUrl + "all"
		i = requests.get(url).json()
		countryOut = novelOutput("Global", i)
	else:
		try:
			url = apiUrl + "countries/" + country
			i = requests.get(url).json()
			countryOut = novelOutput(i['country'], i)
		except:
			countryOut = country + " is not a valid country name. Try again."
	return countryOut

def main():
	helpTable = PrettyTable()
	helpTable.field_names = ["Command", "Description"]
	helpTable.align["Command"] = "l"
	helpTable.align["Description"] = "l"
	helpTable.add_row(["bingus", "Returns United States total statistics"])
	helpTable.add_row(["bingus <XX>/<statename>", "Returns US State statistics using 2-letter abbreviation or full state name"])
	helpTable.add_row(["bingus TopX", "Returns top X US States ranked by total cases (no X defaults to 5)"])
	helpTable.add_row(["bingint", "Returns Global total statistics"])
	helpTable.add_row(["bingint <countryname>", "Returns specific Country statistics using full country name"])
	helpTable.add_row(["bingint TopX", "Returns top X countries ranked by total cases (no X defaults to 5)"])
	ncCommands = f'```\n{helpTable.get_string(title="Novel Covid-19 Lookup Commands")}```'

	client = discord.Client()
	ncUSPattern = re.compile("ncus")
	ncIntPattern = re.compile("ncint")

	@client.event
	async def on_ready():
		print(f"{client.user} has connected to Discord!")
	
	@client.event
	async def on_message(ncLookup):
		if ncLookup.content.lower() == "nchelp":
			await ncLookup.channel.send(ncCommands)
		
		elif ncUSPattern.match(ncLookup.content.lower()):
			getState = ncLookup.content[5:]
			if getState == '':
				print("Country: United States")
				await ncLookup.channel.send(countryStats("United States"))
			elif getState.lower().startswith("top"):
				print(f"Top {getState[3:]} states.")
				await ncLookup.channel.send(getTopX("States",getState[3:]))
			elif len(getState) > 2 and len(getState.split()[0]) == 2:
				getCounty = getState[3:]
				print(f"State: {getState} - County: {getCounty}")
				await ncLookup.channel.send(usCountyStats(getState, getCounty))
			else:
				print(f"State: {getState}")
				await ncLookup.channel.send(usStateStats(getState))
		
		elif ncIntPattern.match(ncLookup.content.lower()):
			getCountry = ncLookup.content[6:].lower()
			if getCountry.lower().startswith("top"):
				print(f"Top {getCountry[3:]} countries.")
				await ncLookup.channel.send(getTopX("Countries", getCountry[3:]))
			else:
				print(f"Country: {getCountry}")
				await ncLookup.channel.send(countryStats(getCountry))
	
	client.run(TOKEN)


if __name__ == '__main__':
	main()
